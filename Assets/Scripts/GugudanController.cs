﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GugudanController : MonoBehaviour {

    public Text answerText;
    public string questionText = "김연아는 농구선수입니까?";
    public bool answerOX = false;

	// Use this for initialization
	void Start () {
        answerText.text = questionText;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ForGugudan()
    {
        for (int i = 2; i <= 9; i++)
        {
            for (int j = 1; j <= 9; j++)
            {
                string answ = i + "x" + j + "=" + (i * j) + " ";
                answerText.text += answ;
            }
            answerText.text += "\n";
        }
    }

    public void WhileGugudan()
    {
        int i = 2;
        int j = 1;

        while (i <= 9)
        {
            j = 1;

            while (j <= 9)
            {
                string answ2 = i + "x" + j + "=" + (i * j) + " ";
                answerText.text += answ2;
                j++;
            }
            answerText.text += "\n";
            i++;
        }
    }

    public void QuizInput (bool inp)
    {
        if(inp == answerOX)
        {
            answerText.text = "정답!";
        } else { answerText.text = "그것도 모르냐 빡대갈아."; }
    }
}
